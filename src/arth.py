import requests
import re
import time

def arth(N, Uo, x, y):
	uo, un = Uo, Uo
	n  = N
	for n in xrange(0, n):
		unp1 = (x + un) + (n * y)
		un   =  unp1
	return unp1

if __name__ == '__main__':
	start_time = time.time()
	r    = requests.Session()
	html = r.get('http://challenge01.root-me.org/programmation/ch1/').text
	searchObj = re.compile('(?<=You must find U<sub>)(?:(?!<).)*', re.I)
	N  = searchObj.findall(html)
	searchObj = re.compile('(?<=U<sub>0</sub> = )(?:(?!\n).)*', re.I)
	Uo = searchObj.findall(html)
	tmp = html.split()
	x , y = int(tmp[12]), int(tmp[20])
	N = int(N[0].encode("utf-8"))
	Uo = int(Uo[0].encode("utf-8"))
	result = str(arth(N, Uo, x, y))
	data = {'result':result}
	print "Executed in : " + str(time.time() - start_time) + " sec.\n sending result through GET Method."
	html = r.get('http://challenge01.root-me.org/programmation/ch1/ep1_v.php?result=', params=data).text
	print html