# -*- coding: latin-1 -*- 
from selenium import webdriver
import os.path
import subprocess
import commands
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import time
import unittest, time, re

#EDIT USER DETAILS :
username = 'Punkachu'
pwd      = 'KimKim300'
#EDIT ID NAME :
login_id = 'var_login'
psw_id   = 'password'


class BotOCR():
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(20)
        self.base_url = "http://challenge01.root-me.org/programmation/ch8/"
        self.verificationErrors = []
 
    def connect(self):
        driver.find_element_by_id(login_id).clear()
        driver.find_element_by_id(login_id).send_keys(username)
        driver.find_element_by_id(psw_id).clear()
        driver.find_element_by_id(psw_id).send_keys(pwd)
        driver.find_element_by_xpath('//input[@value = "Submit"]').click()

    def RunOcr(self):
        commands.getstatusoutput('convert screen.png -crop 264x58+35+98  screen.png')
        p = commands.getstatusoutput('tesseract screen.png stdout')[1].decode("utf8")
        p = p.replace(' ', '').replace('\'', '').replace('_', '').replace('-', '')
        p = p.replace('|', 'i').replace('S', '5').replace('5', 's')
        p = p.encode('ascii',errors='ignore')
        p = re.sub(r'[^\x00-\x7F]',' ', p)
        c = ',.;?:/!§&~#{()'
        for x in c:
            p = p.replace(x, '')
        print p.split()[0]
        s = p.split()[0]
        if (len(s) > 12):
            s = s[1:]
        if (len(s) > 12):
            s = s[:len(s) - 1]
        print s
        return s


    def Hack(self):
        start_time = time.time()

        driver = self.driver
        driver.get(self.base_url)
        self.driver.implicitly_wait(40)

        #make screenshoot of the current page
        self.driver.get_screenshot_as_file('screen.png')

        res = self.RunOcr()
        driver.find_element_by_xpath('//input[@name = "cametu"]').send_keys(res)
        self.driver.get_screenshot_as_file('screen2.png')
        driver.find_element_by_xpath('//input[@value = "Try"]').click()

        self.driver.get_screenshot_as_file('screen3.png')

        print time.time() - start_time
 
 
    def tearDown(self):
        self.driver.quit()
 
if __name__ == "__main__":
    bot = BotOCR()
    for i in xrange(1):
        try:
            bot.setUp()
            bot.Hack()
            #bot.tearDown()
        finally:
            print "next"
    commands.getstatusoutput('display screen.png')